import Statistieken from "./Statistieken.js";

const content = document.querySelector(".content");
const template = document.querySelector("template");
const storage = JSON.parse(localStorage.getItem("transactie"));


function showContent() {
    for (let i = 0; i < storage.length; i++) {
        const clone = template.content.cloneNode(true);
        content.appendChild(clone);
        let bedrag = document.querySelectorAll(".content h1");
        let verzender = document.querySelectorAll(".verzender");
        let ontvanger = document.querySelectorAll(".ontvanger");


        bedrag[i].append(storage[i].bedrag);
        verzender[i].append(storage[i].verzender);
        ontvanger[i].append(storage[i].ontvanger);
    }
}

showContent();



document.querySelector(".gemiddeld").innerHTML = new Statistieken().getSum();
document.querySelector(".minimum").innerHTML = new Statistieken().getMin();
document.querySelector(".maximum").innerHTML = new Statistieken().getMax();

