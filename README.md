# Transactie

**Features**

- Op de webpagina kan een transactie worden gemaakt.
- Bij succes wordt een notificatie weergegeven.
- Gemaakte transacties worden opgeslagen in localStorage.
- Op index.html worden alle opgeslagen transacties overzichtelijk weergegeven, inclusief statistieken.
- U kunt eenvoudig navigeren tussen index.html en add-transaction.html.


De transactiepagina kunt u **[hier](https://cute-naiad-72fed8.netlify.app/)** vinden.