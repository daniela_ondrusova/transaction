export default class Transaction {
    constructor(verzender, bedrag, ontvanger) {
        this.bedrag = bedrag;
        this.verzender = verzender;
        this.ontvanger = ontvanger;
    }
    print() {
        console.log(this.verzender, "sent", this.bedrag, "bits to", this.ontvanger);
    }
}
