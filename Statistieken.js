export default class Statistieken {
    constructor() {
        this.storage = JSON.parse(localStorage.getItem("transactie"));
    }

    getSum() {
        let sum = 0;

        for (let i = 0; i < this.storage.length; i++) {
            sum += parseInt(this.storage[i].bedrag, 10) / this.storage.length;
        }
        return sum;
    }

    getMin() {
        let min = Math.min(...this.storage.map(item => item.bedrag));
        return min;
    }

    getMax() {
        let max = Math.max(...this.storage.map(item => item.bedrag));
        return max;
    }
}
