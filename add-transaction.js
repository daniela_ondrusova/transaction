import Transaction from "./Transaction.js";

let button = document.querySelector("button");
let notificatie = document.querySelector("p");
let bedrag = document.getElementById("bedrag");
let verzender = document.getElementById("verzender");
let ontvanger = document.getElementById("ontvanger");
let trans = JSON.parse(localStorage.getItem("transactie")) || [];


button.addEventListener("click", () => {
    let option = new Transaction(verzender.value, bedrag.value, ontvanger.value);
    option.print();
    notificatie.innerHTML = "Transactie gemaakt!";
    notificatie.style.color = "green";
    bedrag.value = "";
    verzender.value = "";
    ontvanger.value = "";

    if (!localStorage.getItem("transactie")) {
        localStorage.setItem("transactie", JSON.stringify(option));
    }

    trans.push(option);
    localStorage.setItem("transactie", JSON.stringify(trans));
});



let input = document.querySelectorAll("input");
input.forEach((e) => {
    e.addEventListener("click", () => {
        notificatie.innerHTML = "";
    });
});


// for (let i = 0; i < localStorage.transactie.length; i++) {
//     let t = trans[i];
//     console.log(`${t.verzender} sent ${t.bedrag} bits to ${t.ontvanger}`);
// }